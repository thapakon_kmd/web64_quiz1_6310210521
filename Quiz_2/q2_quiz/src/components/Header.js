import {Link} from "react-router-dom"
function Header(){
    return(

        <div id="BG">
            <br/>
            <h1>ยินดีต้อนรับสู่ โปรแกรมจำลองเกรดวิชา Web Programming</h1>
                <div>
                <Link to="/">Home</Link>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Link to="calculate">โปรแกรมคำนวณ</Link>
                </div>
        </div>
    );
}
export default Header;