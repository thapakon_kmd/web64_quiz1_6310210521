import Develop from '../components/Develop'

function Developer(){

    return (
        <div>
            <div align="center">
            <h2>โปรแกรมนี้เป็นการจำลองเกรดให้กับนักศึกษาที่คาดหวังกับรายวิชานี้</h2>
            <br/>
            <h3>*กดปุ่ม โปรแกรมคำนวณ ด้านบน เพื่อคำนวณเกรด*</h3>
            <Develop/>
            </div>
        </div>
    );
}

export default Developer;