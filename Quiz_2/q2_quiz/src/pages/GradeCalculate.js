import Grade from '../components/Grade'
import {useState} from "react";
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

function GradeCalculate(){

    const [name, setName] = useState("");
    const [score, setScore] = useState("");
    const [calculate, setGrade] = useState("");

    function OnNameChange(e){
        setName(e.target.value);
    }

    function calculateGrade(){
        let s = parseFloat(score);
        if(s>=80){
            setGrade("A");
        }else if(s>=75){
            setGrade("B+");
        }else if(s>=70){
            setGrade("B");
        }else if(s>=65){
            setGrade("C+");
        }else if(s>=60){
            setGrade("C");
        }else if(s>=50){
            setGrade("D+");
        }else if(s>=40){
            setGrade("D");
        }else{
            setGrade("E");
        }
    }

    return (
        <div>
            <div align="center">
                <h2> โปรแกรมจำลองเกรด </h2>
                <br/>
                
                <TextField required id="outlined-required" label="ชื่อ" defaultValue="ชื่อ" type="text" value={name} onChange={ (e) => {OnNameChange(e) }}/><br/><br/>
                <TextField required id="outlined-required" label="คะแนน" defaultValue="คะแนน" type="text" value={score} onChange={(e) =>{setScore(e.target.value) }}/><br/><br/>
                <Button variant="contained" onClick={(e) => {calculateGrade()}}>Calculate</Button>
                <hr/>

                <Grade
                    name = {name}
                    score = {score}
                    grade = {calculate}
                />
            </div>
        </div>



    );
}

export default GradeCalculate;