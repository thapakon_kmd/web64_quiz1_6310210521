import './App.css';
import Header from './components/Header'
import { Routes, Route } from "react-router-dom";
import GradeCalculate from './pages/GradeCalculate';
import Developer from './pages/Developer';
function App() {
  return (
    <div className='App'>
      <Header />
      <br/>
      <Routes>
        <Route path="/" element={
                      <Developer />
                    } />
        <Route path="Calculate" element={
                      <GradeCalculate />
                    } />
      </Routes>
    </div>
  );
}

export default App;
