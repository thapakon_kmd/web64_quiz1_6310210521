
function Grade(props){

    return (
        <div>
            <h3>ชื่อ : {props.name}</h3>
            <h3>คะแนน : {props.score}</h3>
            <h3>เกรดที่ได้ : {props.grade}</h3>
        </div>
    );
}

export default Grade;